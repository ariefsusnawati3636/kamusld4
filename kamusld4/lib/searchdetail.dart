import 'dart:convert';

import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:kamusld4/model/api.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  Future<List<Kamus>> getAllkamus() async {
    Uri url = Uri.parse("http://192.168.43.111/kamus/config/Get_kamus.php");

    var res = await http.get(url);
    print(res.body);

    List? data = (json.decode(res.body) as Map<String, dynamic>)["data"];

    if (data == null || data.isEmpty) {
      return [];
    } else {
      return data.map((e) => Kamus.fromJson(e)).toList();
    }
  }

  late List<Kamus> data;
  String kata = '';
  String arti = '';
  String des = '-';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data = Get.find(tag: 'data');
  }

  @override
  // TODO: implement widget
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: EasySearchBar(
            title: const Text('Pencarian'),
            onSearch: (value) {
              if (value.isNotEmpty) {
                getDescByQuery(value);
              } else {
                setState(() {
                  kata = '';
                  arti = '';
                  des = '-';
                });
              }
            }),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(kata).paddingAll(8),
            Text(arti).paddingSymmetric(horizontal: 8),
            Expanded(
              child: Text(des),
              flex: 2,
            )
          ],
        ));
  }

  void getDescByQuery(String query) {
    Kamus hasil = data.where((element) {
      if (element.kata == null) {
        return false;
      } else {
        return element.kata!.contains(query) || element.arti!.contains(query);
      }
    }).first;

    setState(() {
      kata = hasil.kata ?? '';
      arti = hasil.arti ?? '';
      des = hasil.ket ?? '';
    });
  }
}
