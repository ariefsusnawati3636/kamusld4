import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kamusld4/Constants/color.dart';
import 'package:kamusld4/Screens/Login/LoginForm.dart';
import 'package:kamusld4/size_config.dart';
import 'package:kamusld4/utils/constants.dart';
import 'package:simple_shadow/simple_shadow.dart';

class LoginComponent extends StatefulWidget {
  _LoginComponent createState() => _LoginComponent();
}

class _LoginComponent extends State<LoginComponent> {
  @override
  Widget Build(BuildContext context) {
    return SafeArea(
        child: Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenHeight(20)),
      child: SingleChildScrollView(
          child: Column(children: [
        SizedBox(
          height: SizeConfig.screenHeight * 0.04,
        ),
        SizedBox(
          height: SizeConfig.screenHeight * 0.04,
        ),
        SimpleShadow(
          child: Image.asset(
            "assets/images/kamus.png",
            height: 150,
            width: 202,
          ),
          opacity: 0.5,
          color: appPurpleDark,
          offset: Offset(5, 5),
          sigma: 2,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Login",
                style: mTitleStyle,
              )
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        SignInform()
      ])),
    ));
  }

  @override
  dynamic noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}
