import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/route_manager.dart';
import 'package:http/http.dart' as http;
import 'package:kamusld4/Constants/color.dart';
import 'package:kamusld4/listdetail.dart';
import 'package:kamusld4/model/api.dart';
import 'package:lottie/lottie.dart';
import 'package:url_launcher/url_launcher.dart';

class SearchBar extends StatefulWidget {
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  Future<List<Kamus>> getAllkamus() async {
    Uri url = Uri.parse("http://192.168.43.111/kamus/config/Get_kamus.php");
    var res = await http.get(url);

    List? data = (json.decode(res.body) as Map<String, dynamic>)["data"];

    if (data == null || data.isEmpty) {
      return [];
    } else {
      return data.map((e) => Kamus.fromJson(e)).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('tes'),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(context: context, delegate: SearchUser());
            },
          )
        ],
      ),
      body: Container(
        child: FutureBuilder<List<Kamus>>(
            future: getAllkamus(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (!snapshot.hasData) {
                return Center(
                  child: Text("Tidak ada data"),
                );
              }

              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    Kamus kamus = snapshot.data![index];
                    return ListTile(
                      onTap: () {
                        Get.to(ListDetail(), arguments: kamus);
                      },
                      leading: CircleAvatar(
                        backgroundColor: appPurpleLight,
                        child: Text("${kamus.id}"),
                      ),
                      title: Text("${kamus.kata}"),
                      subtitle: Text("${kamus.arti}"),
                      trailing: Text("  "),
                    );
                  });
            }),
      ),
    );
  }
}

class SearchUser extends SearchDelegate {
  var data = [];
  List<Kamus> results = [];
  Future<List<Kamus>> getAllkamus() async {
    Uri url = Uri.parse("http://192.168.43.111/kamus/config/Get_kamus.php");
    var res = await http.get(url);

    List? data = (json.decode(res.body) as Map<String, dynamic>)["data"];

    if (data == null || data.isEmpty) {
      return [];
    } else {
      return data.map((e) => Kamus.fromJson(e)).toList();
    }
  }

  Future<List<Kamus>> getKamusbyquery(String query) async {
    Uri url = Uri.parse("http://192.168.43.111/kamus/config/Get_kamus.php");
    var res = await http.get(url);

    List? data = (json.decode(res.body) as Map<String, dynamic>)["data"];

    if (data == null || data.isEmpty) {
      return [];
    } else {
      var tmp = data.map((e) => Kamus.fromJson(e)).toList();
      var tempKamus =
          tmp.where((element) => element.kata!.contains(query)).first;
      return [tempKamus];
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [IconButton(onPressed: () {}, icon: Icon(Icons.close))];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder<List<Kamus>>(
        future: getKamusbyquery(query),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          List<Kamus>? data = snapshot.data;
          return ListView.builder(
              itemCount: data?.length,
              itemBuilder: (context, index) {
                Kamus kamus = snapshot.data![index];
                return ListTile(
                  onTap: () {
                    Get.to(ListDetail(), arguments: kamus);
                  },
                  leading: CircleAvatar(
                    backgroundColor: appPurpleLight,
                    child: Text('${data?[index].id}'),
                  ),
                  title: Text('${data?[index].kata}'),
                  subtitle: Text('${data?[index].arti}'),
                  trailing: Text("  "),
                );
              });
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Center(
      child: Text('Cari Katamu'),
    );
  }
}
