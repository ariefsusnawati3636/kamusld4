import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const appPurple = Color(0xFF431AA1);
const appPurpleDark = Color(0xff1E0771);
const appPurpleLight = Color(0xFF9345F2);
const appPurpleLight2 = Color(0xFFB9A2D8);
const appWhihte = Color(0xFFFAF8FC);
const appOrenge = Color(0xFFE6704A);

ThemeData appLight = ThemeData(
  primaryColor: appPurple,
  scaffoldBackgroundColor: appWhihte,
  appBarTheme: AppBarTheme(
    backgroundColor: appPurple,
  ),
  textTheme: TextTheme(
    bodyText1: TextStyle(color: appPurpleDark),
    bodyText2: TextStyle(color: appPurpleDark),
  ),
);

ThemeData appDark = ThemeData(
  primaryColor: appPurple,
  scaffoldBackgroundColor: appPurpleDark,
  appBarTheme: AppBarTheme(
    backgroundColor: appPurpleDark,
  ),
  textTheme: TextTheme(
    bodyText1: TextStyle(color: appWhihte),
    bodyText2: TextStyle(color: appWhihte),
  ),
);
