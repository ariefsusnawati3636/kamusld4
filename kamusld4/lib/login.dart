import 'dart:convert';
import 'dart:ffi';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kamusld4/Screens/Login/LoginComponent.dart';
import 'package:kamusld4/main.dart';
import 'package:kamusld4/register.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_login_buttons/social_login_buttons.dart';
import 'mainmenu.dart';
import 'package:kamusld4/size_config.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

enum Loginstatus { notsignIn, signIn }

class _LoginState extends State<Login> {
  Loginstatus _loginstatus = Loginstatus.notsignIn;
  String? username, password;
  final _key = new GlobalKey<FormState>();

  bool _secureText = true;

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  check() {
    final form = _key.currentState;
    if (form!.validate()) {
      form.save();
      login();
    }
  }

  login() async {
    try {
      final response = await http.post(
          Uri.parse("http://192.168.43.111/kamus/api/login.php"),
          body: {"username": username, "password": password});
      final data = jsonDecode(response.body);
      int value = data['value'];
      String pesan = data['message'];
      String namaAPI = data['nama'];
      String usernameAPI = data['username'];
      if (value == 1) {
        const snackBar = SnackBar(
          content: Text('Berhasil'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        setState(() {
          _loginstatus = Loginstatus.signIn;
          savePref(value, usernameAPI, namaAPI);
        });
        print(pesan);
      } else {
        const snackBar = SnackBar(
          content: Text('gagal!'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    } catch (e) {
      const snackBar = SnackBar(
        content: Text('gagal'),
      );

      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  savePref(int value, String username, String nama) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", value);
      preferences.setString("nama", nama);
      preferences.setString("username", username);
      preferences.commit();
    });
  }

  var value;
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      value = preferences.getInt("value");
      _loginstatus = value == 1 ? Loginstatus.signIn : Loginstatus.notsignIn;
    });
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("value", 0);
      preferences.commit();
      _loginstatus = Loginstatus.notsignIn;
    });
  }

  checkLogin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.getInt("value") == 1) {
      setState(() {
        _loginstatus = Loginstatus.signIn;
      });
    } else {
      setState(() {
        _loginstatus = Loginstatus.notsignIn;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    switch (_loginstatus) {
      case Loginstatus.notsignIn:
        return Scaffold(
          appBar: AppBar(
            title: Text(
              "Login",
              style: const TextStyle(),
            ),
            centerTitle: true,
            automaticallyImplyLeading: false,
            backgroundColor: Color(0xff1E0771),
          ),
          body: Form(
            key: _key,
            child: ListView(
              padding: EdgeInsets.all(19.0),
              children: <Widget>[
                ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.asset(
                      "assets/images/kamus4.png",
                      width: 400,
                      height: 300,
                    )),
                TextFormField(
                  validator: (e) {
                    if (e!.isEmpty) {
                      return "please insert username";
                    }
                  },
                  onSaved: (e) => username = e,
                  decoration: InputDecoration(
                    labelText: "Username",
                    prefixIcon: Icon(Icons.people),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  validator: (e) {
                    if (e!.isEmpty) {
                      return "silakan diisi Password";
                    }
                  },
                  obscureText: _secureText,
                  onSaved: (e) => password = e,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.lock),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    labelText: "Password",
                    suffixIcon: IconButton(
                      onPressed: showHide,
                      icon: Icon(_secureText
                          ? Icons.visibility_off
                          : Icons.visibility),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: 100,
                  height: 45,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Color(0xff1E0771),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    onPressed: () {
                      check();
                    },
                    child: Text(
                      "Masuk",
                      style: TextStyle(
                        color: Color(0xffffffff),
                      ),
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => Register()));
                  },
                  child: Text("Buat Akun Baru",
                      style: TextStyle(color: Color(0xff1E0771))),
                ),
              ],
            ),
          ),
        );
        break;
      case Loginstatus.signIn:
        return MainMenu(signOut);
        break;
    }
  }
}
