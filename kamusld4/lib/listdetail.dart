import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'Constants/color.dart';

class ListDetail extends StatefulWidget {
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<ListDetail> {
  final kamus = Get.arguments;
  @override
  // TODO: implement widget
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${kamus.arti}'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                  colors: [
                    appPurpleLight,
                    appPurpleDark,
                  ],
                ),
              ),
              child: Material(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(20),
                child: InkWell(
                  onTap: () {},
                  borderRadius: BorderRadius.circular(20),
                  child: Container(
                    child: Stack(
                      children: [
                        Positioned(
                          bottom: -90,
                          right: -28,
                          child: Opacity(
                            opacity: 0.6,
                            child: Container(
                              width: 350,
                              height: 300,
                              child: Image.asset(
                                "assets/images/kamus4.png",
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Icon(
                                    Icons.menu_book_rounded,
                                    color: appWhihte,
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    '${kamus.kata}',
                                    style: TextStyle(
                                      color: appWhihte,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 30),
                              Text(
                                "Arti",
                                style: TextStyle(
                                  color: appWhihte,
                                ),
                              ),
                              SizedBox(height: 20),
                              Text(
                                '${kamus.arti}',
                                style: TextStyle(
                                  fontSize: 25,
                                  color: appWhihte,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
