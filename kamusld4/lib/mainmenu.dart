import 'dart:convert';
import 'dart:ffi';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:kamusld4/Constants/color.dart';
import 'package:kamusld4/controller/home_controller.dart';
import 'package:kamusld4/main.dart';
import 'package:kamusld4/model/api.dart';
import 'package:kamusld4/searchDetailtest.dart';
import 'package:kamusld4/searchdetail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'SearchBar.dart';
import 'lastread.dart';
import 'listdetail.dart';

class MainMenu extends StatefulWidget {
  final VoidCallback signOut;
  MainMenu(this.signOut);
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  List<Kamus> data = [];
  Ambil() async {
    Uri url = Uri.parse("http://192.168.43.111/kamus/config/Get_kamus.php");
    var res = await http.get(url);
  }

  signOut() {
    setState(() {
      widget.signOut();
    });
  }

  String username = "", nama = "";

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      username = preferences.getString("username")!;
      nama = preferences.getString("nama")!;
    });
  }

  void initState() {
    super.initState();
    getPref();
  }

  Future<List<Kamus>> getAllkamus() async {
    Uri url = Uri.parse("http://192.168.43.111/kamus/config/Get_kamus.php");
    var res = await http.get(url);

    List? data = (json.decode(res.body) as Map<String, dynamic>)["data"];

    if (data == null || data.isEmpty) {
      return [];
    } else {
      return data.map((e) => Kamus.fromJson(e)).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: appLight,
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            "Kamus Besemah",
            style: const TextStyle(),
          ),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: () {
                Get.put(data, tag: 'data');
                Get.to(SearchBar());
              },
              icon: Icon(Icons.search),
            ),
          ],
        ),
        body: DefaultTabController(
          length: 2,
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Selamat Datang $nama",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(
                      colors: [
                        appPurpleLight,
                        appPurpleDark,
                      ],
                    ),
                  ),
                  child: Material(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(20),
                    child: InkWell(
                      // onTap: () => Get.to(DetailLastRead()),
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        child: Stack(
                          children: [
                            Positioned(
                              bottom: -90,
                              right: -28,
                              child: Opacity(
                                opacity: 0.6,
                                child: Container(
                                  width: 350,
                                  height: 300,
                                  child: Image.asset(
                                    "assets/images/kamus4.png",
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.menu_book_rounded,
                                        color: appWhihte,
                                      ),
                                      SizedBox(width: 10),
                                      Text(
                                        "Tentang Aplikasi",
                                        style: TextStyle(
                                          color: appWhihte,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 30),
                                  Text(
                                    "Kamus Besemah :",
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: appWhihte,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Kamus ini berguna untuk mencari kata-kata dalam bahasa besemah ke indonesia begitu juga sebaliknya",
                                    style: TextStyle(
                                      color: appWhihte,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                TabBar(
                  indicatorColor: appPurpleDark,
                  labelColor: appPurpleDark,
                  unselectedLabelColor: Colors.grey,
                  tabs: [
                    Tab(
                      text: "List",
                    ),
                    Tab(
                      text: "Bantuan",
                    ),
                  ],
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      FutureBuilder<List<Kamus>>(
                          future: getAllkamus(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                            if (!snapshot.hasData) {
                              return Center(
                                child: Text("Tidak ada data"),
                              );
                            }
                            if (snapshot.hasData) {
                              data = snapshot.requireData;
                            }
                            return ListView.builder(
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, index) {
                                  Kamus kamus = snapshot.data![index];
                                  return ListTile(
                                    onTap: () =>
                                        Get.to(ListDetail(), arguments: kamus),
                                    leading: CircleAvatar(
                                      backgroundColor: appPurpleLight,
                                      child: Text("${kamus.id}"),
                                    ),
                                    title: Text("${kamus.kata}"),
                                    subtitle: Text("${kamus.arti}"),
                                    trailing: Text("  "),
                                  );
                                });
                          }),
                      Center(
                        child: Material(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                colors: [
                                  appPurpleLight,
                                  appPurpleLight,
                                ],
                              ),
                            ),
                            child: Container(
                              width: 400,
                              height: 400,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Contact Me :",
                                    style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "No. Ponsel: 0877xxxxx",
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Email: megi@gmail.com",
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 80,
                                  ),
                                  Text(
                                    "Kamus Besemah",
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xff1E0771),
                                    ),
                                  ),
                                  Text(
                                    "version 1.0",
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xff1E0771),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
            hoverColor: Colors.amber,
            child: Icon(Icons.exit_to_app),
            onPressed: () {
              signOut();
            }),
      ),
    );
  }
}
