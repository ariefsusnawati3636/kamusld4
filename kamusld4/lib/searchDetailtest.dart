import 'dart:convert';
import 'dart:core';
import 'dart:ffi';
import 'dart:math';

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:easy_search_bar/easy_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kamusld4/Constants/color.dart';
import 'package:kamusld4/listdetail.dart';
import 'package:kamusld4/model/api.dart';
import 'package:http/http.dart' as http;
import 'package:kamusld4/Constants/color.dart';

class Posts {
  final String id;
  final String kata;
  final String arti;
  final String ket;
  final String gambar;

  Posts(
      {required this.id,
      required this.kata,
      required this.arti,
      required this.ket,
      required this.gambar});

  factory Posts.formJson(Map<String, dynamic> json) {
    return Posts(
      id: json['id'],
      kata: json['kata'],
      arti: json['arti'],
      ket: json['ket'],
      gambar: json['gambar'],
    );
  }
}

class SearchTest extends StatefulWidget {
  _SearchTestState createState() => _SearchTestState();
}

class _SearchTestState extends State<SearchTest> {
  // ignore: prefer_final_fields, unused_field
  List<Posts> _list = [];
  List<Posts> _search = [];
  var loading = false;

  Future<Null> fetchData() async {
    setState(() {});
    _list.clear();
    final response = await http
        .get(Uri.parse("http://192.168.43.111/kamus/config/Get_kamus.php"));
    print('ini jalan');
    if (Response == Null) {
      final data = (json.decode(response.body));
      setState(() {
        for (Map<String, dynamic> i in data) {
          _list.add(Posts.formJson(i));
          // _list.add(Posts.formJson(i));
          print('ini ngelist');
        }
      });
    }
  }

  TextEditingController controller = new TextEditingController();

  onSearch(String text) async {
    _search.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _list.forEach((f) {
      if (f.kata.contains(text) || f.arti.toString().contains(text))
        // ignore: curly_braces_in_flow_control_structures
        _search.add(f);
    });

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10.0),
              color: Colors.blue,
              child: Card(
                child: ListTile(
                  leading: Icon(Icons.search),
                  title: TextField(
                    controller: controller,
                    onChanged: onSearch,
                    decoration: InputDecoration(
                        hintText: "Search", border: InputBorder.none),
                  ),
                  trailing: IconButton(
                    onPressed: () {
                      controller.clear();
                      onSearch('');
                    },
                    icon: Icon(Icons.cancel),
                  ),
                ),
              ),
            ),
            Expanded(
              child: _search.length != 0 || controller.text.isNotEmpty
                  ? ListView.builder(
                      itemCount: _search.length,
                      itemBuilder: (context, i) {
                        final b = _search[i];
                        return Container(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  b.kata,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                Text(b.arti),
                              ],
                            ));
                      },
                    )
                  : ListView.builder(
                      itemCount: _list.length,
                      itemBuilder: (context, i) {
                        final a = _list[i];
                        return Container(
                            padding: EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  a.kata,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                Text(a.arti),
                              ],
                            ));
                      },
                    ),
            ),
          ],
        ),
      ),
    );
  }
}

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     backgroundColor: Color(0xff1E0771),
  //     appBar: AppBar(
  //       backgroundColor: Color(0xff1E0771),
  //       elevation: 0.0,
  //     ),
  //     body: Padding(
  //       padding: EdgeInsets.all(16),
  //       child: Column(
  //         mainAxisAlignment: MainAxisAlignment.start,
  //         children: [
  //           Text(
  //             "kata",
  //             style: TextStyle(
  //               color: Colors.white,
  //               fontSize: 25,
  //               fontWeight: FontWeight.bold,
  //             ),
  //           ),
  //           SizedBox(
  //             height: 20.0,
  //           ),
  //           TextField(
  //             style: TextStyle(color: Colors.white),
  //             decoration: InputDecoration(
  //               filled: true,
  //               fillColor: Color(0xFF9345F2),
  //               border: OutlineInputBorder(
  //                 borderRadius: BorderRadius.circular(8.0),
  //                 borderSide: BorderSide.none,
  //               ),
  //               hintText: "eg: The Dark Night",
  //               prefixIcon: Icon(Icons.search),
  //               prefixIconColor: Colors.purple.shade800,
  //             ),
  //           ),
  //           SizedBox(
  //             height: 20.0,
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
// / }
