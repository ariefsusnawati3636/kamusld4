class Kamus {
  Kamus({
    this.id,
    this.kata,
    this.arti,
    this.ket,
    this.gambar,
  });

  String? id;
  String? kata;
  String? arti;
  String? ket;
  String? gambar;

  factory Kamus.fromJson(Map<String, dynamic> json) => Kamus(
        id: json["id"] == null ? null : json["id"],
        kata: json["kata"] == null ? null : json["kata"],
        arti: json["arti"] == null ? null : json["arti"],
        ket: json["ket"] == null ? null : json["ket"],
        gambar: json["gambar"] == null ? null : json["gambar"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "kata": kata == null ? null : kata,
        "arti": arti == null ? null : arti,
        "ket": ket == null ? null : ket,
        "gambar": gambar == null ? null : gambar,
      };
}

/////////////////////////////
class KamusDua {
  String? id;
  String? kata;
  String? arti;
  String? ket;
  String? gambar;

  KamusDua({
    this.id,
    this.kata,
    this.arti,
    this.ket,
    this.gambar,
  });

  KamusDua.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    kata = json['kata'];
    arti = json['arti'];
    ket = json['ket'];
    gambar = json['gambar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['kata'] = this.kata;
    data['arti'] = this.arti;
    data['gambar'] = this.gambar;
    return data;
  }
}
// /gggggg

class MovieModel {
  String? id;
  String? kata;
  String? arti;
  String? ket;
  String? gambar;

  MovieModel(this.id, this.kata, this.arti, this.ket, this.gambar);
}
